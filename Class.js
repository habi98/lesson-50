    class Machine {
        constructor(){
         this._enabled = false;
        };
        turnOn() {
            this._enabled  = true;
            console.log('Включен')
        };

        turnOff() {
            this._enabled = false;
            console.log('выключен');
        };

    };

    class HomeAppliance extends  Machine {
     constructor() {
         super();
    };
        plugIn() {
           this.enableNetwork = true;
            console.log('Включен в сеть');
        };
        plugOff() {
            this.enableNetwork = false;
            console.log('Выключен из сети')
        };

    };
    class WashingMachine extends HomeAppliance{
    constructor(){
        super();
        this.wateAmout = 0;
    };
        run() {
            console.log('Cтиральная Машина запустилась');
        };
    };
    class LightSource extends HomeAppliance{
    constructor() {
        super();
    };
        setLevel(level){
            this.level = level;
            if(level < 1 || level <= 100){
                console.log(`Уровень Освещенности"${level}`);
            };
        };

    };
    class AutoVehicle extends Machine{
        constructor() {
            super();
            this.x = 0;
            this.y = 0;
        };
        setPosition(x,y) {
            this.x = x;
            this.y = y;

        };
    };
    class Car extends AutoVehicle  {
        constructor(){
            super()
            this.speed = 10;
        }
        setSpeed(speed){
            console.log(`машина движется со скоростью ${speed} `)
    };

    run (x,y) {
        const interval = setInterval(() => {
            let newX = this.x + this.speed;
            if(newX >= x) {
                newX = x;
            };

            let newY = this.y + this.speed;
            if(newY >= y) {
                newY = y;
            }

            this.setPosition(newX, newY);
            console.log('Машина движется ' + newX + ' - ' + newY)
            if(newX === x && newY === y) {
                clearInterval(interval);
            }
        }, 1000)
    };

    };



